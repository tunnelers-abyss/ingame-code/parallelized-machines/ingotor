-- ######################################################
--               Ingotor Main controller 
--         by Guill4um on august, 21th of 2020
-- ######################################################

-- this controller is for settings and starting the machine


-- ** settings **
local LCD_CHANNEL = "LCD"
local PIN_GRIND = "C" -- set the good pin in UPPERCASE ex : "B"
function is_grinding() return pin.c end --set the good pin here too in lowercase ex: pin.b
local PIN_BAKE = "B" -- set the good pin in UPPERCASE ex : "B"
function is_baking() return pin.b end --set the good pin here too in lowercase ex: pin.b
local PIN_START = "A" -- set the good pin in UPPERCASE ex : "B"


-- ** functions **
-- display a message on the lcd
function display(message, ...)
    digiline_send(LCD_CHANNEL, message:format(...))
end

-- function that can be used in this controller or on digiline or on interrupt
function processing(arg)
    if arg.step == "listing" then
        if arg.action == "TAKE_NEXT" then
            digiline_send("injector_input",{})
        end
    end
end

-- the function start orr rrestart the machine
local function restart() 
    if mem.machine_is_running then 
        processing({step = "listing", action = "TAKE_NEXT"})
    end
end

-- function that update the lcd message dependings on the settings
local function update_lcd(mode) 
    mode = mode or ""
    local grinding = "";
    local baking = "";
    local spacer = "";
    if is_grinding() then grinding = "GRIND" end
    if is_baking() then baking = "BAKE" end
    if not is_baking() and not is_grinding() then grinding = "Please select to start" end
    if  is_baking() and is_grinding() then spacer = " . " end
    display("%s%s%s%s%s",mode,spacer,grinding,spacer,baking )
end


-- ** events **
if event.type == "program" then 
    -- reset the machine after clicking on execute
    digiline_send("settings","CLEAR")
    interrupt(2,"update_lcd")  
elseif event.type == "on" then 
    -- start the machine if it's allowed
    if (event.pin.name == PIN_START and mem.machine_is_running == nil) then
        update_lcd(".Processing. ")
        if is_baking() or  is_grinding() then
            digiline_send("settings","test_started")
            digiline_send("settings",tostring(is_baking()).."baking")
            digiline_send("settings",tostring(is_grinding()).."grinding")
            digiline_send("settings","PLAYER_DETECT")
            mem.machine_is_running = true
            restart() 
        end
    end
elseif event.type == "digiline" then
    -- execute process stuff
    if event.channel == "processing" then 
        processing(event.msg)
    -- update lcd after a reset
    elseif event.channel == "settings" then 
        if event.msg == "CLEAR" then
            interrupt(2,"update_lcd")  
        end
    end
elseif event.type == "interrupt" then
    -- update ldc after a reset
    if event.iid == "update_lcd" then
        mem.machine_is_running = nil
        update_lcd("> Ready to : ")
    end
end

if event.type == "on" or event.type == "off"  then 
    -- update lcd when changing settings when machine is ready
    if (not mem.machine_is_running) then
        update_lcd("> Ready to : ")
    end
end